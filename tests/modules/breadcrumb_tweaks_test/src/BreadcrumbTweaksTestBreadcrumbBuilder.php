<?php

namespace Drupal\breadcrumb_tweaks_test;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Test breadcrumb builder.
 */
class BreadcrumbTweaksTestBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();

    $size = \Drupal::state()->get('breadcrumb_tweaks_test_size');
    $links = [];
    for ($i = 0; $i < $size; $i++) {
      $links[$i] = Link::createFromRoute("link_$i", '<front>');
    }

    $breadcrumb->setLinks($links);
    return $breadcrumb;
  }

}
