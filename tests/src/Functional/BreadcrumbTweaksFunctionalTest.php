<?php

namespace Drupal\Tests\breadcrumb_tweaks\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional testing of Breadcrumb Tweaks
 *
 * @group breadcrumb_tweaks
 */
class BreadcrumbTweaksFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'system',
    'breadcrumb_tweaks_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

  }

  /**
   * Tests removing intermediate elements from breadcrumbs.
   */
  public function testRemoveIntermediateElements() {
    $page = $this->getSession()->getPage();

    // Instruct our builder to build a breadcrumb with 5 links.
    \Drupal::state()->set('breadcrumb_tweaks_test_size', 5);

    $this->drupalGet('<front>');
    $li_elements = $page->findAll('css', 'nav li');
    $this->assertSame(5, count($li_elements));
    $this->assertSame('link_0', $li_elements[0]->getText());
    $this->assertSame('link_1', $li_elements[1]->getText());
    $this->assertSame('...', $li_elements[2]->getText());
    $this->assertSame('link_3', $li_elements[3]->getText());
    $this->assertSame('link_4', $li_elements[4]->getText());

    drupal_flush_all_caches();

    // Instruct our builder to build a breadcrumb with 4 links, no ellipsis
    // should be there.
    \Drupal::state()->set('breadcrumb_tweaks_test_size', 4);

    $this->drupalGet('<front>');
    $li_elements = $page->findAll('css', 'nav li');
    $this->assertSame(4, count($li_elements));
    $this->assertSame('link_0', $li_elements[0]->getText());
    $this->assertSame('link_1', $li_elements[1]->getText());
    $this->assertSame('link_2', $li_elements[2]->getText());
    $this->assertSame('link_3', $li_elements[3]->getText());
  }

}
